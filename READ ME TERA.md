Vous pouvez lister les noms des comptes actifs :
gcloud auth list

Vous pouvez lister les ID de projet :
gcloud config list project

Ouvrez un nouvel onglet Cloud Shell et vérifiez si Terraform est disponible :
terraform

créez un fichier de configuration vide nommé instance.tf :
touch instance.tf

Cliquez sur le fichier instance.tf et ajoutez-y le contenu suivant:
resource "google_compute_instance" "terraform" {
project      = "NOM DU PROJET"
name         = "terraform"
machine_type = "e2-medium"
zone         = ""
boot_disk {
initialize_params {
image = "debian-cloud/debian-11"
}
}
network_interface {
network = "default"
access_config {
}
}
##}


vérifiez que le fichier que nous avons créé a bien été ajouté et que votre répertoire ne contient aucun autre fichier *.tf, puisque Terraform charge tous les fichiers stockés dans ce répertoire :
ls

Téléchargez et installez le binaire du fournisseur :
terraform init

Créez un plan d'exécution :
terraform plan

Dans le répertoire contenant le fichier instance.tf que vous avez créé, exécutez cette commande :
terraform apply

inspectez l'état actuel :
terraform show
