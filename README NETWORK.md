# PROJECT_M2I_GIT

gcloud auth list : Affiche la liste des comptes d'authentification actuellement connectés à la CLI Google Cloud.

gcloud config list project: Affiche le projet actuellement configuré dans votre environnement de développement Google Cloud.

gcloud config set compute/zone "us-central1-b"
export ZONE=$(gcloud config get compute/zone) : Coonfigurer la zone.

gcloud config set compute/region "us-central1"
export REGION=$(gcloud config get compute/region): récuperer la zone configuré.

•	Pour créer le réseau personnalisé :
gcloud compute networks create taw-custom-network --subnet-mode custom

Permet de créer des sous-réseaux pour notre nouveau réseau personnalisé, dans notre cas trois :
1.	Créez "subnet-us-central1" avec un préfixe IP :
gcloud compute networks subnets create subnet-us-central1 \
   --network taw-custom-network \
   --region us-central1 \
   --range 10.0.0.0/16

2.	Créez "subnet-us-east1" avec un préfixe IP :
gcloud compute networks subnets create subnet-us-east1 \
   --network taw-custom-network \
   --region us-east1 \
   --range 10.1.0.0/16

3.	Créez "subnet-europe-west4" avec un préfixe IP :
gcloud compute networks subnets create subnet-europe-west4 \
   --network taw-custom-network \
   --region europe-west4 \
   --range 10.2.0.0/16

4.	Affiche la liste de nos réseaux :
gcloud compute networks subnets list \
   --network taw-custom-network

•	Pour créer des règles de pare-feu:
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http





Les 4 commandes permettent de créer des règles, pour les différents protocole :

•	ICMP
Champ	Valeur	Commentaires
Nom	nw101-allow-icmp	Nom de la nouvelle règle
Cibles	Tags cibles spécifiés	À sélectionner dans la liste déroulante "Cibles"
Tags cibles	règles	tag
Filtre source	Plages IPv4	Nous allons ouvrir le pare-feu à toutes les adresses IP de cette liste.
Plages IPv4 sources	0.0.0.0/0	Nous allons ouvrir le pare-feu à toutes les adresses IP provenant d'Internet.
Protocoles et ports	Sélectionnez Protocoles et ports spécifiés, Autres protocoles, puis saisissez icmp.	Protocoles et ports auxquels s'applique le pare-feu
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules

•	Communications internes
Champ	Valeur	Commentaires
Nom	nw101-allow-internal	Nom de la nouvelle règle
Cibles	Toutes les instances du réseau	À sélectionner dans la liste déroulante "Cibles"
Filtre source	Plages IPv4	Filtre permettant d'appliquer la règle à des sources de trafic spécifiques
Plages IPv4 sources	10.0.0.0/16,
10.1.0.0/16,
10.2.0.0/16	Nous allons ouvrir le pare-feu à toutes les adresses IP provenant d'Internet.
Protocoles et ports	Sélectionnez Protocoles et ports spécifiés, puis tcp, et saisissez 0-65535 ; cochez la case udp, saisissez 0-65535 ; cochez la case Autres protocoles, puis saisissez icmp.	Autorise Tcp:0-65535, udp:0-65535 et ICMP
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"




SSH
Champ	Valeur	Commentaires
Nom	nw101-allow-ssh	Nom de la nouvelle règle
Cibles	Tags cibles spécifiés	ssh
Tags cibles	ssh	Instances auxquelles vous appliquez la règle de pare-feu
Filtre source	Plages IPv4	Filtre permettant d'appliquer la règle à des sources de trafic spécifiques
Plages IPv4 sources	0.0.0.0/0	Nous allons ouvrir le pare-feu à toutes les adresses IP provenant d'Internet.
Protocoles et ports	Sélectionnez Protocoles et ports spécifiés, cochez la case tcp, puis saisissez 22.	Autorise tcp:22
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"

•	RDP
Champ	Valeur	Commentaires
Nom	nw101-allow-rdp	Nom de la nouvelle règle
Cibles	Toutes les instances du réseau	À sélectionner dans la liste déroulante "Cibles"
Filtre source	Plages IPv4	Filtrer les adresses IP
Plages IPv4 sources	0.0.0.0/0	Nous allons ouvrir le pare-feu à toutes les adresses IP provenant d'Internet.
Protocoles et ports	Sélectionnez Protocoles et ports spécifiés, cochez la case tcp, puis saisissez 3389.	Autorise tcp:3389
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"


1.	P créer une instance nommée us-test-01 dans le sous-réseau "subnet-us-central1" :
gcloud compute instances create us-test-01 \
--subnet subnet-us-central1 \
--zone us-central1-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules
2.	Permet de créer les VM "us-test-02" et "us-test-03" dans les sous-réseaux correspondants :
gcloud compute instances create us-test-02 \
--subnet subnet-us-east1 \
--zone us-east1-d \
--machine-type e2-standard-2 \
--tags ssh,http,rules


gcloud compute instances create us-test-03 \
--subnet subnet-europe-west4 \
--zone europe-west4-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules

Permet de tester l’accessibilité :
ping -c 3 <us-test-02-external-ip-address>
ping -c 3 <us-test-03-external-ip-address>

sudo apt-get update : mise à jour système

sudo apt-get -y install traceroute mtr tcpdump iperf whois host dnsutils siege : télecharger les packets « traceroute » utilise à notre système

traceroute www.icann.org : permet d’effectuer une route vers le site.

